import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.css']
})
export class PostListItemComponentComponent implements OnInit {

  @Input() post = Object;
  
  constructor() { }
  
  ngOnInit() {
  }

  like() {
    this.post.loveIts++;
  }
  
  dislike() {
    this.post.loveIts--;
  }
}
